The project has all the libraries from Hadoop 2.2 in the lib directory, and they have already been added to the build path.

Cloning the project and importing it in Eclipse is all you need to do to start working with it

Command:
hadoop jar <JAR_NAME> inputPath transiveclosurePath cartesianproductPath
