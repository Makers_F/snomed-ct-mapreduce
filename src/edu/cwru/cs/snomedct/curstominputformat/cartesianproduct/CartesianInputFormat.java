package edu.cwru.cs.snomedct.curstominputformat.cartesianproduct;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.join.CompositeInputSplit;
import org.apache.hadoop.mapreduce.task.JobContextImpl;
import org.apache.hadoop.util.ReflectionUtils;
import org.apache.hadoop.util.StringUtils;

public class CartesianInputFormat<KEYIN, VALUEIN> extends FileInputFormat<Text, Text> {

	public static final String LEFT_INPUT_FORMAT = "cart.left.inputformat";
	public static final String LEFT_INPUT_PATH = "cart.left.path";

	public static final String RIGHT_INPUT_FORMAT = "cart.right.inputformat";
	public static final String RIGHT_INPUT_PATH = "cart.right.path";

	public static final String CARTESIAN_INPUT_ELEMENT_SEPARATOR = "cart.recordreader.separator";
	public static final String CARTESIAN_SKIP_NON_SORTED_OPTIMIZZATION = "cart.recordreader.skip_non_sorted";


	public static void setSkipNonSortedOptimizzation(JobContext job, boolean value) {
		Configuration conf = job.getConfiguration();
		conf.setBoolean(CARTESIAN_SKIP_NON_SORTED_OPTIMIZZATION, value);
	}

	public static void setSeparator(JobContext job, String character) {
		Configuration conf = job.getConfiguration();
		conf.set(CARTESIAN_INPUT_ELEMENT_SEPARATOR, character);
	}

	public static void setLeftInputInfo(JobContext job, Class<? extends FileInputFormat<?, ?>> inputFormat) {
		Configuration conf = job.getConfiguration();
		conf.setClass(LEFT_INPUT_FORMAT, inputFormat, FileInputFormat.class);
	}

	public static void setRightInputInfo(JobContext job, Class<? extends FileInputFormat<?, ?>> inputFormat) {
		Configuration conf = job.getConfiguration();
		conf.setClass(RIGHT_INPUT_FORMAT, inputFormat, FileInputFormat.class);
	}

	public static void addLeftInputPath(JobContext job, Path path) throws IOException {
		Configuration conf = job.getConfiguration();
		Path temp_path = path.getFileSystem(conf).makeQualified(path);
		String dirStr = StringUtils.escapeString(temp_path.toString());
		String dirs = conf.get(LEFT_INPUT_PATH);
		conf.set(LEFT_INPUT_PATH, dirs == null ? dirStr : dirs + "," + dirStr);
	}

	public static void addRightInputPath(JobContext job, Path path) throws IOException {
		Configuration conf = job.getConfiguration();
		Path temp_path = path.getFileSystem(conf).makeQualified(path);
		String dirStr = StringUtils.escapeString(temp_path.toString());
		String dirs = conf.get(RIGHT_INPUT_PATH);
		conf.set(RIGHT_INPUT_PATH, dirs == null ? dirStr : dirs + "," + dirStr);
	}
	  
	@Override
	public List<InputSplit> getSplits(JobContext context) throws IOException {
		// Get the input splits from both the left and right data sets
		List<InputSplit> leftSplits;
		List<InputSplit> rightSplits;

		leftSplits = getInputSplits(context, LEFT_INPUT_FORMAT, LEFT_INPUT_PATH);
		rightSplits = getInputSplits(context, RIGHT_INPUT_FORMAT, RIGHT_INPUT_PATH);

		// Create our CompositeInputSplits, size equal to
		// left.length * right.length

		List<InputSplit> returnSplits = new ArrayList<InputSplit>(leftSplits.size() * rightSplits.size());

		CompositeInputSplit cis;
		// For each of the left input splits
		for (InputSplit left : leftSplits) {
			// For each of the right input splits
			for (InputSplit right : rightSplits) {
				// Create a new composite input split composing of the two
				cis = new CompositeInputSplit(2);
				try {
					cis.add(left);
					cis.add(right);
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
				returnSplits.add(cis);
			}
		}
		return returnSplits;

	}

	private List<InputSplit> getInputSplits(JobContext jobConf, String inputFormatClassID, String inputPathsID) throws IOException {
		// Create a new instance of the input format
		Configuration conf = jobConf.getConfiguration();
		@SuppressWarnings("unchecked")
		FileInputFormat<KEYIN, VALUEIN> inputFormat = ReflectionUtils.newInstance(conf.getClass(inputFormatClassID, null, FileInputFormat.class), conf);
		Configuration copyConf = new Configuration(conf);
		copyConf.set(INPUT_DIR, conf.get(inputPathsID));
		JobContext jobContext = new JobContextImpl(copyConf, jobConf.getJobID());
		
		return inputFormat.getSplits(jobContext);
	}
	  
	@Override
	public RecordReader<Text, Text> createRecordReader(InputSplit split,
			TaskAttemptContext context) throws IOException, InterruptedException {
		CartesianRecordReader<KEYIN, VALUEIN>  cartesianRecordReader= new CartesianRecordReader<KEYIN, VALUEIN>();
		cartesianRecordReader.initialize(split, context);
		return cartesianRecordReader;
	}
	
}
