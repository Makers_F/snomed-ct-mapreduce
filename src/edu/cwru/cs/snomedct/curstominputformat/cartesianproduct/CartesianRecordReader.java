package edu.cwru.cs.snomedct.curstominputformat.cartesianproduct;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.join.CompositeInputSplit;
import org.apache.hadoop.util.ReflectionUtils;

public class CartesianRecordReader<KEYIN, VALUEIN> extends RecordReader<Text, Text>{

	private String separator;
	private boolean skip_non_sorted_optimizzation;
	// Record readers to get key value pairs
	private RecordReader<KEYIN, VALUEIN> leftRR = null, rightRR = null;

	// Store configuration to re-create the right record reader
	private FileInputFormat<KEYIN,VALUEIN> rightFIF;
	private InputSplit rightIS;
	private TaskAttemptContext rightTaskAttemptContext;

	KEYIN lkey = null;
	VALUEIN lvalue = null;
	// Helper variables
	private Text currentKey = new Text();
	private Text currentValue = new Text();
	private boolean goToNextLeft = true, alldone = false;

	@SuppressWarnings("unchecked")
	@Override
	public void initialize(InputSplit split, TaskAttemptContext context)
			throws IOException, InterruptedException {
		CompositeInputSplit compSplit = (CompositeInputSplit) split;
		Configuration conf = context.getConfiguration();

		separator = conf.get(CartesianInputFormat.CARTESIAN_INPUT_ELEMENT_SEPARATOR, "/");
		skip_non_sorted_optimizzation = conf.getBoolean(CartesianInputFormat.CARTESIAN_SKIP_NON_SORTED_OPTIMIZZATION, false);

		InputSplit leftIS = compSplit.get(0);
		this.rightIS = compSplit.get(1);
		this.rightTaskAttemptContext = context;

		// Create left record reader
		FileInputFormat<KEYIN, VALUEIN> leftFIF = ReflectionUtils.newInstance(
				conf.getClass(CartesianInputFormat.LEFT_INPUT_FORMAT,null, FileInputFormat.class), conf);

		leftRR = leftFIF.createRecordReader(leftIS, context);
		leftRR.initialize(leftIS, context);

		// Create right record reader
		rightFIF = ReflectionUtils.newInstance(
				conf.getClass(CartesianInputFormat.RIGHT_INPUT_FORMAT,null, FileInputFormat.class), conf);

		rightRR = rightFIF.createRecordReader(rightIS, context);
		rightRR.initialize(rightIS, context);
	}

	@Override
	public boolean nextKeyValue() throws IOException, InterruptedException {
		do {
			KEYIN rkey = null;
			VALUEIN rvalue = null;
			// If we are to go to the next left key/value pair
			if (goToNextLeft) {
				// Read the next key value pair, false means no more pairs
				if (leftRR.nextKeyValue()) {
					// If we aren't done, set the value to the key and set
					// our flags
					lkey = leftRR.getCurrentKey();
					lvalue = leftRR.getCurrentValue();
					goToNextLeft = alldone = false;

					// Reset the right record reader
					this.rightRR = this.rightFIF.createRecordReader(this.rightIS, this.rightTaskAttemptContext);
					this.rightRR.initialize(this.rightIS, this.rightTaskAttemptContext);
				} else {
					// If no more on the left, then this task is finished
					alldone = true;
					return false;
				}
			}

			// Read the next key value pair from the right data set
			if (rightRR.nextKeyValue()) {
				rkey = rightRR.getCurrentKey();
				rvalue = rightRR.getCurrentValue();
				// If success, set the value
				if(skip_non_sorted_optimizzation && lkey.toString().compareTo(rkey.toString()) >= 0) {
					//ignore the pair in which the rkey is "bigger" than lkey.
					continue;
				}
				currentKey.set(lkey.toString() + separator + rkey.toString());
				currentValue.set(lvalue.toString() + separator + rvalue.toString());

				return true;
			} else {
				// Otherwise, this right data set is complete
				// and we should go to the next left pair
				goToNextLeft = true;
			}

			// This loop will continue if we finished reading key/value
			// pairs from the right data set
		} while (goToNextLeft);

		// Return true if a key/value pair was read, false otherwise
		return !alldone;
	}

	@Override
	public Text getCurrentKey() throws IOException, InterruptedException {
		return currentKey;
	}

	@Override
	public Text getCurrentValue() throws IOException, InterruptedException {
		return currentValue;
	}

	@Override
	public float getProgress() throws IOException, InterruptedException {
		return leftRR.getProgress();
	}

	@Override
	public void close() throws IOException {
		leftRR.close();
		rightRR.close();
	}

}
