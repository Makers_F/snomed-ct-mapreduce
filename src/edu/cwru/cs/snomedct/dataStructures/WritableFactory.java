package edu.cwru.cs.snomedct.dataStructures;

import org.apache.hadoop.io.Text;

public interface WritableFactory<T> {
	public T newInstance();

	public static final WritableFactory<Text> TextWritableFactory = new WritableFactory<Text>() {
		@Override
		public Text newInstance() {
			return new Text();
		}
	};
}
