package edu.cwru.cs.snomedct.dataStructures;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.Writable;

public abstract class ListWritable<T extends Writable> implements Writable {
	
	private List<T> mList;
	private final WritableFactory<T> mFactory;

	public ListWritable(List<T> list, WritableFactory<T> factory) {
		mList = list;
		mFactory = factory;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		if(mList != null) {
			out.writeInt(mList.size());
			for(Writable w : mList) {
				w.write(out);
			}
		} else {
			out.writeInt(-1);
		}
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		int size = in.readInt();
		if(size != -1) {
			List<T> list = new ArrayList<T>(size);
			for(int i= 0; i < size; i++) {
				T element = mFactory.newInstance();
				element.readFields(in);
				list.add(element);
			}
			mList = list;
		} else {
			mList = null;
		}
	}

	public List<T> getList() {
		return mList;
	}
}
