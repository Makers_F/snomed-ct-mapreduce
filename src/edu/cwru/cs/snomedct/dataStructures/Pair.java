package edu.cwru.cs.snomedct.dataStructures;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

public class Pair<K extends Writable, V extends Writable> implements Writable {

	private K key;
	private V value;
	private WritableFactory<K> keyFactory;
	private WritableFactory<V> valuaFactory;

	public Pair(K pKey, V pValue, WritableFactory<K> pKeyFactory, WritableFactory<V> pValueFactory) {
		key = pKey;
		value = pValue;
		keyFactory = pKeyFactory;
		valuaFactory = pValueFactory;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		key.write(out);
		value.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		key = keyFactory.newInstance();
		key.readFields(in);
		value = valuaFactory.newInstance();
		value.readFields(in);
	}

	public K getKey() {
		return key;
	}

	public void setKey(K key) {
		this.key = key;
	}

	public V getValue() {
		return value;
	}

	public void setValue(V value) {
		this.value = value;
	}

}
