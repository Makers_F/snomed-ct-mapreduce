package edu.cwru.cs.snomedct.dataStructures;

import java.util.LinkedList;
import java.util.List;

import org.apache.hadoop.io.Text;

public class TextList extends ListWritable<Text>{
	
	public TextList(List<Text> list) {
		super(list, WritableFactory.TextWritableFactory);
	}

	public TextList() {
		this(new LinkedList<Text>());
	}
}
