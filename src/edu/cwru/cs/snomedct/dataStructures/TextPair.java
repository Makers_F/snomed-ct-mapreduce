package edu.cwru.cs.snomedct.dataStructures;

import org.apache.hadoop.io.Text;

public class TextPair extends Pair<Text, Text> {

	public TextPair(Text pKey, Text pValue) {
		super(pKey, pValue, WritableFactory.TextWritableFactory, WritableFactory.TextWritableFactory);
	}

	public TextPair() {
		this(new Text(), new Text());
	}

}
