package edu.cwru.cs.snomedct.mapreducers.ancestorStep;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.Reducer;


public class TCtoAncestorReducer extends Reducer<Text, Text, Text, Text>{

	public static final String UPPER_BOUND_ELEMENT_SEPARATOR = "upperbound.reducer.separator";
	public static final String UPPER_BOUND_ELEMENT_DEFAULT_SEPARATOR = ";";

	public static void setUpperBoundElementSeparator(JobContext job, String separator) {
		Configuration conf = job.getConfiguration();
		conf.set(UPPER_BOUND_ELEMENT_SEPARATOR, separator);
	}

	@Override
	protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
		StringBuilder builder = new StringBuilder();
		String separator = context.getConfiguration().get(UPPER_BOUND_ELEMENT_SEPARATOR, UPPER_BOUND_ELEMENT_DEFAULT_SEPARATOR);
		for(Text t : values) {
			builder.append(t.toString()).append(separator);
		}
		builder.setLength(builder.length() - 1);
		context.write(key, new Text(builder.toString()));
	}
	

}
