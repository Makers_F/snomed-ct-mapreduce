package edu.cwru.cs.snomedct;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import edu.cwru.cs.snomedct.curstominputformat.cartesianproduct.CartesianInputFormat;
import edu.cwru.cs.snomedct.intersect.IntersectMapper;
import edu.cwru.cs.snomedct.intersect.IntersectReducer;
import edu.cwru.cs.snomedct.mapreducers.ancestorStep.TCtoAncestorMapper;
import edu.cwru.cs.snomedct.mapreducers.ancestorStep.TCtoAncestorReducer;

public class SNOMEDCTDriver extends Configured implements Tool {

	private static final boolean FIND_UPPER_BOUND_FROM_TC = false;
	private static final boolean CARTESIAN_PRODUCT = false;
	private static final boolean FIND_INTERSECT_UPPER_BOUND = true;
	

	@Override
	public int run(String[] args) throws Exception {
		Configuration conf = getConf();

		FileSystem fs = FileSystem.get(conf);

		/*
		 * #################################################################################
		 * #################### ALTERNATIVE FIND UPPER BOUND JOB ###########################
		 * #################################################################################
		 */

		if(FIND_UPPER_BOUND_FROM_TC) {
			fs.delete(new Path(args[1]), true);

			Job find_upper_bound_from_tc_job = Job.getInstance(conf, "find_upper_bound_from_tc");
			find_upper_bound_from_tc_job.setJarByClass(TCtoAncestorMapper.class);

			find_upper_bound_from_tc_job.setMapOutputKeyClass(Text.class);
			find_upper_bound_from_tc_job.setMapOutputValueClass(Text.class);

			find_upper_bound_from_tc_job.setOutputKeyClass(Text.class);
			find_upper_bound_from_tc_job.setOutputValueClass(Text.class);

			find_upper_bound_from_tc_job.setMapperClass(TCtoAncestorMapper.class);
			find_upper_bound_from_tc_job.setReducerClass(TCtoAncestorReducer.class);

			find_upper_bound_from_tc_job.setInputFormatClass(TextInputFormat.class);
			find_upper_bound_from_tc_job.setOutputFormatClass(TextOutputFormat.class);

			TCtoAncestorReducer.setUpperBoundElementSeparator(find_upper_bound_from_tc_job, ",");
			FileInputFormat.addInputPath(find_upper_bound_from_tc_job, new Path(args[0]));
			FileOutputFormat.setOutputPath(find_upper_bound_from_tc_job, new Path(args[1]));

			find_upper_bound_from_tc_job.waitForCompletion(true);
		}

		if(CARTESIAN_PRODUCT) {
			fs.delete(new Path(args[2]), true);

			Job cartesian_product = Job.getInstance(conf, "cartesian_product");
			cartesian_product.setJarByClass(CartesianInputFormat.class);

			cartesian_product.setOutputKeyClass(Text.class);
			cartesian_product.setOutputValueClass(Text.class);

			cartesian_product.setMapperClass(Mapper.class);
			cartesian_product.setNumReduceTasks(0);

			cartesian_product.setInputFormatClass(CartesianInputFormat.class);
			CartesianInputFormat.setLeftInputInfo(cartesian_product, KeyValueTextInputFormat.class);
			CartesianInputFormat.setRightInputInfo(cartesian_product, KeyValueTextInputFormat.class);
			CartesianInputFormat.setSeparator(cartesian_product, "/");
			CartesianInputFormat.addLeftInputPath(cartesian_product, new Path(args[1]));
			CartesianInputFormat.addRightInputPath(cartesian_product, new Path(args[1]));
			CartesianInputFormat.setSkipNonSortedOptimizzation(cartesian_product, true);
			cartesian_product.setOutputFormatClass(TextOutputFormat.class);
			FileOutputFormat.setOutputPath(cartesian_product, new Path(args[2]));
			

			cartesian_product.waitForCompletion(true);
		}
		
		if(FIND_INTERSECT_UPPER_BOUND) {
			fs.delete(new Path(args[3]), true);

			Job find_intersect_upper_bound = Job.getInstance(conf, "find_intersect_upper_bound");
			find_intersect_upper_bound.setJarByClass(IntersectMapper.class);

			find_intersect_upper_bound.setMapOutputKeyClass(Text.class);
			find_intersect_upper_bound.setMapOutputValueClass(Text.class);

			find_intersect_upper_bound.setOutputKeyClass(Text.class);
			find_intersect_upper_bound.setOutputValueClass(Text.class);

			find_intersect_upper_bound.setMapperClass(IntersectMapper.class);
			find_intersect_upper_bound.setReducerClass(IntersectReducer.class);

			find_intersect_upper_bound.setInputFormatClass(TextInputFormat.class);
			find_intersect_upper_bound.setOutputFormatClass(TextOutputFormat.class);
			
			FileInputFormat.addInputPath(find_intersect_upper_bound, new Path(args[2]));
			FileOutputFormat.setOutputPath(find_intersect_upper_bound, new Path(args[3]));

			find_intersect_upper_bound.waitForCompletion(true);
		}


		return 1;
	}

	public static void main(String[] args) throws Exception {
		// Let ToolRunner handle generic command-line options
		int res = ToolRunner.run(new Configuration(), new SNOMEDCTDriver(), args);

		System.exit(res);
	}

}