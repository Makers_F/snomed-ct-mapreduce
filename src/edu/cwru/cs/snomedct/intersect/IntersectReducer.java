package edu.cwru.cs.snomedct.intersect;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import com.google.common.collect.Sets;


public class IntersectReducer extends Reducer<Text, Text, Text, Text>{
	
	public static String INTERSECT_UPPER_BOUNDS = "";
	
	public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
		StringBuilder builder = new StringBuilder();
		for(Text t : values) {
			builder.append(t.toString());
		}
		String s = builder.toString();
		String[] str = s.split("/");
		String[] UB1 = str[0].split(",");
		String[] UB2 = str[1].split(",");

	    Set<String> UPPERBOUND1 = new HashSet<String>();
	    for(int i = 0; i < UB1.length; i++){
	           UPPERBOUND1.add(UB1[i]);
	    }
	    Set<String> UPPERBOUND2 = new HashSet<String>();
		for(int i = 0; i < UB2.length; i++){
		       UPPERBOUND2.add(UB2[i]);
		}
		
		StringBuilder build = new StringBuilder();
		
		Set<String> INTERSECT = Sets.intersection(UPPERBOUND1, UPPERBOUND2);
		
		if(INTERSECT.size()>0){
			for(int i = 0; i < INTERSECT.size(); i++){
				build.append(INTERSECT.iterator().next().toString()).append(",");
			}
			build.setLength(build.length() - 1);
			INTERSECT_UPPER_BOUNDS = build.toString();
		}
				
		context.write(key, new Text(INTERSECT_UPPER_BOUNDS));
	
	}
}