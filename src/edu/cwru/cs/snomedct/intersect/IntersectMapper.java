package edu.cwru.cs.snomedct.intersect;

import java.io.IOException;


import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class IntersectMapper extends Mapper<LongWritable, Text, Text, Text> {


	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		//some of the input line only contains \t, at this circumstance, the reducer wont work
		if(value.toString().length() > 1){
		String[] values = value.toString().split("\t");
		context.write(new Text(values[0]), new Text(values[1]));
		}
	}
}

